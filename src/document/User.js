import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'
import passportLocalMongoose from 'passport-local-mongoose'
import config from 'config'
import UrlHelper from './../helper/UrlHelper.js'
import CManager from './../service/ConnectionManager.js'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    connection = CManager.get(UrlHelper.build([config.application.user, config.application.project]))
    ;

var User = new Schema({
    name: {
        first: String,
        last: String
    },
    login: String,
    role: {type: String, default: 'user'},
    projects: [
        {
            project: {type: ObjectId, ref: "Project"},
            permission: {type: String, default: 'owner'}
        }
    ]
}, {collection: '__user'});

User.plugin(timestamps);
User.plugin(passportLocalMongoose, {
    usernameUnique: true
});

User.methods.toJSON = function() {
    var obj = this.toObject();
    delete obj.salt;
    delete obj.hash;
    return obj
};

export default connection.model('User', User);