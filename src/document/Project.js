import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'
import config from './../config'
import CManager from './../service/ConnectionManager.js'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Project = new Schema({
    name: String,
    owner: {type: ObjectId, ref: 'User'},
    link: String,
    runner: {
        user: String,
        project: String,
        secret: String
    },
    type: String,
    description: String
}, {collection: "__projects"});

Project.plugin(timestamps);

export default CManager.application.model('Project', Project);
export function make(connection) {
    connection.model('Project', Project)
}
export const TYPES = {
    LOCAL: 'local',
    REMOTE: 'remote'
};