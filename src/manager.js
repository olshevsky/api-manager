require("source-map-support").install();
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__dirname) {/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _express = __webpack_require__(1);
	
	var _express2 = _interopRequireDefault(_express);
	
	var _config = __webpack_require__(2);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _child_process = __webpack_require__(6);
	
	var _path = __webpack_require__(7);
	
	var _path2 = _interopRequireDefault(_path);
	
	var _serveFavicon = __webpack_require__(8);
	
	var _serveFavicon2 = _interopRequireDefault(_serveFavicon);
	
	var _morgan = __webpack_require__(9);
	
	var _morgan2 = _interopRequireDefault(_morgan);
	
	var _cookieParser = __webpack_require__(10);
	
	var _cookieParser2 = _interopRequireDefault(_cookieParser);
	
	var _bodyParser = __webpack_require__(11);
	
	var _bodyParser2 = _interopRequireDefault(_bodyParser);
	
	var _fs = __webpack_require__(5);
	
	var _fs2 = _interopRequireDefault(_fs);
	
	var _middlewareResponseJs = __webpack_require__(12);
	
	var _middlewareResponseJs2 = _interopRequireDefault(_middlewareResponseJs);
	
	var _middlewareServerErrorJs = __webpack_require__(13);
	
	var _middlewareServerErrorJs2 = _interopRequireDefault(_middlewareServerErrorJs);
	
	var _middlewareNotFoundJs = __webpack_require__(17);
	
	var _middlewareNotFoundJs2 = _interopRequireDefault(_middlewareNotFoundJs);
	
	var _middlewareProxyJs = __webpack_require__(18);
	
	var _middlewareProxyJs2 = _interopRequireDefault(_middlewareProxyJs);
	
	var _http = __webpack_require__(26);
	
	var _socketIo = __webpack_require__(27);
	
	var _socketIo2 = _interopRequireDefault(_socketIo);
	
	var _serviceBootstrapJs = __webpack_require__(28);
	
	var _serviceBootstrapJs2 = _interopRequireDefault(_serviceBootstrapJs);
	
	var _serviceProcessManagerJs = __webpack_require__(20);
	
	var _serviceProcessManagerJs2 = _interopRequireDefault(_serviceProcessManagerJs);
	
	var _serviceLoggerJs = __webpack_require__(21);
	
	var _serviceLoggerJs2 = _interopRequireDefault(_serviceLoggerJs);
	
	var _serviceInstanceLoggerJs = __webpack_require__(24);
	
	var _serviceInstanceLoggerJs2 = _interopRequireDefault(_serviceInstanceLoggerJs);
	
	var port = _config2['default'].application.port;
	
	var app = (0, _express2['default'])();
	var server = (0, _http.Server)(app);
	var io = (0, _socketIo2['default'])(server);
	
	io.on('connection', function (socket) {});
	
	app.set('views', _path2['default'].join(__dirname, 'views'));
	app.set('view engine', 'jade');
	
	app.get('/', function (req, res, next) {
	    res.render('index');
	});
	
	app.use(_middlewareResponseJs2['default']);
	app.use(__webpack_require__(36)());
	app.use(_middlewareProxyJs2['default']);
	app.use(_bodyParser2['default'].json());
	app.use(_bodyParser2['default'].urlencoded({ extended: false }));
	
	app.get('/restart', function (req, res, next) {
	    Object.keys(_serviceProcessManagerJs2['default'].list).forEach(function (p) {
	        _serviceProcessManagerJs2['default'].restart(p);
	    });
	
	    res.json({ success: true });
	});
	
	app.get('/logs', function (req, res, next) {
	    res.json(_serviceInstanceLoggerJs2['default'].get());
	});
	
	app.get('/management', function (req, res, next) {
	    res.json(Object.keys(_serviceProcessManagerJs2['default'].list).map(function (p) {
	        return {
	            p: _serviceProcessManagerJs2['default'].list[p].port
	        };
	    }));
	});
	
	app.get('/api/manager/restart/:user/:project', function (req, res, next) {
	    var processName = _serviceProcessManagerJs2['default'].getProcessName(req.params.user, req.params.project);
	    if (_serviceProcessManagerJs2['default'].has(processName)) {
	        if (_serviceProcessManagerJs2['default'].restart(processName)) {
	            res.json({ success: true, data: { message: 'Process ' + processName + ' has been restarted!' } });
	        } else {
	            res.json({ success: false, data: { message: 'Process ' + processName + ' can not be restarted!!' } });
	        }
	    } else {
	        res.json({ success: false, data: { message: 'Process ' + processName + ' not found!' } });
	    }
	});
	
	_serviceBootstrapJs2['default'].init().then(function (projects) {
	    var p = projects.map(function (project) {
	        return {
	            name: project.runner.project,
	            secret: project.runner.secret
	        };
	    });
	});
	
	app.use(_middlewareNotFoundJs2['default']);
	app.use(_middlewareServerErrorJs2['default']);
	var p = port || 3001;
	
	_serviceLoggerJs2['default'].logger.info({ success: true, message: "Manager has been started", metadata: { port: p } });
	server.listen(p);
	/* WEBPACK VAR INJECTION */}.call(exports, "src"))

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = require("express");

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _applicationJs = __webpack_require__(3);
	
	var _applicationJs2 = _interopRequireDefault(_applicationJs);
	
	var _applicationLocalJs = __webpack_require__(4);
	
	var _applicationLocalJs2 = _interopRequireDefault(_applicationLocalJs);
	
	var _fs = __webpack_require__(5);
	
	var _fs2 = _interopRequireDefault(_fs);
	
	function merge(o1, o2) {
	    Object.keys(o2).forEach(function (prop) {
	        var val = o2[prop];
	        if (o1.hasOwnProperty(prop)) {
	            if (typeof val == 'object') {
	                if (val && val.constructor != Array) {
	                    val = merge(o1[prop], val);
	                }
	            }
	        }
	        o1[prop] = val;
	    });
	    return o1;
	}
	exports['default'] = merge(_applicationJs2['default'], _applicationLocalJs2['default']);
	module.exports = exports['default'];

/***/ },
/* 3 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports["default"] = {
	    application: {
	        id: "pockemon_manager",
	        user: "pockemon_manager",
	        project: "api",
	        version: 1,
	        port: 3000
	    },
	    manager: {
	        start: './node_modules/api-core/bin/api-core'
	    }
	};
	module.exports = exports["default"];

/***/ },
/* 4 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports['default'] = {
	    centrifuge: {
	        path: './../../Work/Pockemon/centrifugo-0.2.4-linux-amd64',
	        web: './../../Work/Pockemon/centrifugo-0.2.4-linux-amd64/web-master/app'
	    },
	    application: {
	        port: 9000
	    },
	    manager: {
	        start: './../api-core/bin/api-core.js'
	    }
	};
	module.exports = exports['default'];

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = require("fs");

/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = require("child_process");

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = require("path");

/***/ },
/* 8 */
/***/ function(module, exports) {

	module.exports = require("serve-favicon");

/***/ },
/* 9 */
/***/ function(module, exports) {

	module.exports = require("morgan");

/***/ },
/* 10 */
/***/ function(module, exports) {

	module.exports = require("cookie-parser");

/***/ },
/* 11 */
/***/ function(module, exports) {

	module.exports = require("body-parser");

/***/ },
/* 12 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	exports["default"] = function (req, res, next) {
	    res.response = function (response) {
	        response.response = res;
	        response.send();
	    };
	
	    next();
	};
	
	module.exports = exports["default"];

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _responseErrorResponseJs = __webpack_require__(14);
	
	var _responseErrorResponseJs2 = _interopRequireDefault(_responseErrorResponseJs);
	
	exports['default'] = function (err, req, res, next) {
	    res.response(new _responseErrorResponseJs2['default'](err.message, {
	        stack: err.stack
	    }, 500, 500));
	};
	
	module.exports = exports['default'];

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var _JsonResponseJs = __webpack_require__(15);
	
	var _JsonResponseJs2 = _interopRequireDefault(_JsonResponseJs);
	
	var ErrorResponse = (function (_JsonResponse) {
	    _inherits(ErrorResponse, _JsonResponse);
	
	    function ErrorResponse(message, data, success, status) {
	        _classCallCheck(this, ErrorResponse);
	
	        _get(Object.getPrototypeOf(ErrorResponse.prototype), 'constructor', this).call(this, data, success, status);
	
	        this.message = message;
	    }
	
	    _createClass(ErrorResponse, [{
	        key: 'data',
	        get: function get() {
	            return {
	                message: this.message,
	                data: this._data,
	                success: this._success
	            };
	        }
	    }]);
	
	    return ErrorResponse;
	})(_JsonResponseJs2['default']);
	
	exports['default'] = ErrorResponse;
	module.exports = exports['default'];

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var _ResponseJs = __webpack_require__(16);
	
	var _ResponseJs2 = _interopRequireDefault(_ResponseJs);
	
	var JsonResponse = (function (_Response) {
	    _inherits(JsonResponse, _Response);
	
	    function JsonResponse(data, success, status) {
	        _classCallCheck(this, JsonResponse);
	
	        _get(Object.getPrototypeOf(JsonResponse.prototype), 'constructor', this).call(this, data, status);
	        this._success = success;
	    }
	
	    _createClass(JsonResponse, [{
	        key: 'send',
	        value: function send() {
	            this._response.status(this.status).json(this.data);
	        }
	    }, {
	        key: 'data',
	        get: function get() {
	            return {
	                data: this._data,
	                success: this._success
	            };
	        }
	    }]);
	
	    return JsonResponse;
	})(_ResponseJs2['default']);
	
	exports['default'] = JsonResponse;
	module.exports = exports['default'];

/***/ },
/* 16 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Response = (function () {
	    function Response(data, status) {
	        _classCallCheck(this, Response);
	
	        this._data = data;
	        this.status = status;
	    }
	
	    _createClass(Response, [{
	        key: "send",
	        value: function send() {
	            this._response.status(this.status).send(this._data);
	        }
	    }, {
	        key: "response",
	        set: function set(req) {
	            this._response = req;
	        }
	    }, {
	        key: "data",
	        get: function get() {}
	    }]);
	
	    return Response;
	})();
	
	exports["default"] = Response;
	module.exports = exports["default"];

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _responseErrorResponseJs = __webpack_require__(14);
	
	var _responseErrorResponseJs2 = _interopRequireDefault(_responseErrorResponseJs);
	
	exports['default'] = function (req, res, next) {
	    var err = new Error('Not Found');
	    err.status = 404;
	    res.response(new _responseErrorResponseJs2['default']("Not Found", {}, false, 404));
	};
	
	module.exports = exports['default'];

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _httpProxy = __webpack_require__(19);
	
	var _serviceProcessManagerJs = __webpack_require__(20);
	
	var _serviceProcessManagerJs2 = _interopRequireDefault(_serviceProcessManagerJs);
	
	var _https = __webpack_require__(25);
	
	__webpack_require__(26);
	
	var _bodyParser = __webpack_require__(11);
	
	var _bodyParser2 = _interopRequireDefault(_bodyParser);
	
	exports['default'] = function (req, res, next) {
	    var a = req.url.split('/');
	    if (a.length >= 5) {
	        var part = '/' + a[1] + '/' + a[2];
	        if (_serviceProcessManagerJs2['default'].has(part)) {
	            var proxy = (0, _httpProxy.createServer)({});
	            var instance = _serviceProcessManagerJs2['default'].get(part);
	            proxy.web(req, res, {
	                target: instance.hostname,
	                ws: true
	            });
	        } else {
	            next();
	        }
	    } else {
	        next();
	    }
	};
	
	module.exports = exports['default'];

/***/ },
/* 19 */
/***/ function(module, exports) {

	module.exports = require("http-proxy");

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _child_process = __webpack_require__(6);
	
	var _config = __webpack_require__(2);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _LoggerJs = __webpack_require__(21);
	
	var _LoggerJs2 = _interopRequireDefault(_LoggerJs);
	
	var _InstanceLoggerJs = __webpack_require__(24);
	
	var _InstanceLoggerJs2 = _interopRequireDefault(_InstanceLoggerJs);
	
	var ProcessManager = (function () {
	    function ProcessManager() {
	        _classCallCheck(this, ProcessManager);
	
	        this.list = {};
	    }
	
	    _createClass(ProcessManager, [{
	        key: 'get',
	        value: function get(part) {
	            if (!this.list.hasOwnProperty(part)) {
	                this.create(part);
	            }
	
	            return this.list[part];
	        }
	    }, {
	        key: 'has',
	        value: function has(part) {
	            return this.list.hasOwnProperty(part);
	        }
	    }, {
	        key: 'restart',
	        value: function restart(processName) {
	            if (this.has(processName)) {
	                try {
	                    var APIApp = this.get(processName);
	
	                    APIApp.process.kill();
	                    delete APIApp.process;
	
	                    var env = {
	                        port: APIApp.port,
	                        project: APIApp.project,
	                        user: APIApp.user,
	                        url: APIApp.url
	                    };
	
	                    APIApp.process = ProcessManager.getProcess(env);
	
	                    return true;
	                } catch (e) {
	                    return false;
	                }
	            } else {
	                return false;
	            }
	        }
	    }, {
	        key: 'getProcessName',
	        value: function getProcessName(user, project) {
	            return '/' + user + '/' + project;
	        }
	    }, {
	        key: 'create',
	        value: function create(user, project, port) {
	            var url = this.getProcessName(user, project);
	
	            var env = {
	                url: url,
	                project: project,
	                user: user,
	                port: port
	            };
	            var apiProcess = ProcessManager.getProcess(env);
	
	            this.list[url] = {
	                hostname: 'http://' + process.env.HOSTNAME + ':' + port,
	                process: apiProcess,
	                project: project,
	                user: user,
	                url: url,
	                logs: [],
	                port: port
	            };
	
	            apiProcess.stdout.on('data', function (data) {
	                _LoggerJs2['default'].logger.info(data.toString());
	                var parsed = {};
	                try {
	                    parsed = JSON.parse(data.toString());
	                } catch (e) {}
	                _InstanceLoggerJs2['default'].add(url, parsed);
	            });
	            apiProcess.stderr.on("data", function (data) {
	                _LoggerJs2['default'].logger.error(data.toString());
	                var parsed = {};
	                try {
	                    parsed = JSON.parse(data.toString());
	                } catch (e) {}
	                _InstanceLoggerJs2['default'].add(url, parsed);
	            });
	
	            return apiProcess;
	        }
	    }], [{
	        key: 'getProcess',
	        value: function getProcess(env) {
	            return (0, _child_process.spawn)('node', [_config2['default'].manager.start], { env: env });
	        }
	    }]);
	
	    return ProcessManager;
	})();
	
	exports['default'] = new ProcessManager();
	module.exports = exports['default'];

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/14/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _winston = __webpack_require__(22);
	
	var _winston2 = _interopRequireDefault(_winston);
	
	var _moment = __webpack_require__(23);
	
	var _moment2 = _interopRequireDefault(_moment);
	
	var Logger = function Logger() {
	    _classCallCheck(this, Logger);
	
	    this.logger = new _winston2['default'].Logger({
	        transports: [new _winston2['default'].transports.Console()]
	    });
	};
	
	exports['default'] = new Logger();
	module.exports = exports['default'];

/***/ },
/* 22 */
/***/ function(module, exports) {

	module.exports = require("winston");

/***/ },
/* 23 */
/***/ function(module, exports) {

	module.exports = require("moment");

/***/ },
/* 24 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/28/15.
	 */
	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var InstanceLogger = (function () {
	    function InstanceLogger() {
	        _classCallCheck(this, InstanceLogger);
	
	        this.instances = {};
	    }
	
	    _createClass(InstanceLogger, [{
	        key: "add",
	        value: function add(instance, message) {
	            this.instances[instance] = this.instances[instance] || [];
	            this.instances[instance].push(message);
	        }
	    }, {
	        key: "get",
	        value: function get() {
	            return this.instances;
	        }
	    }]);
	
	    return InstanceLogger;
	})();
	
	exports["default"] = new InstanceLogger();
	module.exports = exports["default"];

/***/ },
/* 25 */
/***/ function(module, exports) {

	module.exports = require("https");

/***/ },
/* 26 */
/***/ function(module, exports) {

	module.exports = require("http");

/***/ },
/* 27 */
/***/ function(module, exports) {

	module.exports = require("socket.io");

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _promise = __webpack_require__(29);
	
	var _promise2 = _interopRequireDefault(_promise);
	
	var _localPort = __webpack_require__(30);
	
	var _localPort2 = _interopRequireDefault(_localPort);
	
	var _documentProjectJs = __webpack_require__(31);
	
	var _documentProjectJs2 = _interopRequireDefault(_documentProjectJs);
	
	var _ProcessManagerJs = __webpack_require__(20);
	
	var _ProcessManagerJs2 = _interopRequireDefault(_ProcessManagerJs);
	
	var _moment = __webpack_require__(23);
	
	var _moment2 = _interopRequireDefault(_moment);
	
	var Bootstrap = (function () {
	    function Bootstrap() {
	        _classCallCheck(this, Bootstrap);
	    }
	
	    _createClass(Bootstrap, null, [{
	        key: 'init',
	        value: function init() {
	            return new _promise2['default'](function (resolve, reject) {
	                _documentProjectJs2['default'].find().exec(function (err, projects) {
	                    projects.map(function (project) {
	                        _localPort2['default'].findOpen(3008, function (err, port) {
	                            if (err) {
	                                console.error(err);
	                            } else {
	                                var user = project.runner.user;
	
	                                var projectId = project.runner.project;
	                                _ProcessManagerJs2['default'].create(user, projectId, port);
	                            }
	                        });
	                    });
	
	                    resolve(projects);
	                });
	            });
	        }
	    }]);
	
	    return Bootstrap;
	})();
	
	exports['default'] = Bootstrap;
	module.exports = exports['default'];

/***/ },
/* 29 */
/***/ function(module, exports) {

	module.exports = require("promise");

/***/ },
/* 30 */
/***/ function(module, exports) {

	module.exports = require("local-port");

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports.make = make;
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _mongoose = __webpack_require__(32);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var _mongooseTimestamp = __webpack_require__(33);
	
	var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);
	
	var _config = __webpack_require__(2);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _serviceConnectionManagerJs = __webpack_require__(34);
	
	var _serviceConnectionManagerJs2 = _interopRequireDefault(_serviceConnectionManagerJs);
	
	var Schema = _mongoose2['default'].Schema,
	    ObjectId = Schema.ObjectId;
	
	var Project = new Schema({
	    name: String,
	    owner: { type: ObjectId, ref: 'User' },
	    link: String,
	    runner: {
	        user: String,
	        project: String,
	        secret: String
	    },
	    type: String,
	    description: String
	}, { collection: "__projects" });
	
	Project.plugin(_mongooseTimestamp2['default']);
	
	exports['default'] = _serviceConnectionManagerJs2['default'].application.model('Project', Project);
	
	function make(connection) {
	    connection.model('Project', Project);
	}
	
	var TYPES = {
	    LOCAL: 'local',
	    REMOTE: 'remote'
	};
	exports.TYPES = TYPES;

/***/ },
/* 32 */
/***/ function(module, exports) {

	module.exports = require("mongoose");

/***/ },
/* 33 */
/***/ function(module, exports) {

	module.exports = require("mongoose-timestamp");

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _mongoose = __webpack_require__(32);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var _config = __webpack_require__(2);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _helperUrlHelperJs = __webpack_require__(35);
	
	var _helperUrlHelperJs2 = _interopRequireDefault(_helperUrlHelperJs);
	
	var ConnectionManager = (function () {
	    function ConnectionManager() {
	        _classCallCheck(this, ConnectionManager);
	
	        this.connections = {};
	    }
	
	    _createClass(ConnectionManager, [{
	        key: 'get',
	        value: function get(url) {
	            if (!this.connections.hasOwnProperty(url)) {
	                this.connections[url] = _mongoose2['default'].createConnection(ConnectionManager.getUrl(url));
	            }
	
	            return this.connections[url];
	        }
	    }, {
	        key: 'application',
	        get: function get() {
	            return this.get(_helperUrlHelperJs2['default'].build([_config2['default'].application.user, _config2['default'].application.project]));
	        }
	    }], [{
	        key: 'getUrl',
	        value: function getUrl(collection) {
	            var credentials = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];
	
	            if (!credentials) {
	                return 'mongodb://localhost/' + collection;
	            } else {
	                return 'mongodb://localhost/' + collection;
	            }
	        }
	    }]);
	
	    return ConnectionManager;
	})();
	
	exports['default'] = new ConnectionManager();
	module.exports = exports['default'];

/***/ },
/* 35 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var UrlHelper = (function () {
	    function UrlHelper() {
	        _classCallCheck(this, UrlHelper);
	    }
	
	    _createClass(UrlHelper, null, [{
	        key: 'build',
	        value: function build(params) {
	            return Array.isArray(params) ? params.join('_') : 'default_';
	        }
	    }]);
	
	    return UrlHelper;
	})();
	
	exports['default'] = UrlHelper;
	module.exports = exports['default'];

/***/ },
/* 36 */
/***/ function(module, exports) {

	module.exports = require("cors");

/***/ }
/******/ ]);
//# sourceMappingURL=manager.js.map