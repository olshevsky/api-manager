/**
 * Created by igor.olshevsky on 8/13/15.
 */

class UrlHelper {
    static build(params) {
        return Array.isArray(params) ? params.join('_') : 'default_'
    }
}

export default UrlHelper