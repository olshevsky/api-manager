/**
 * Created by igor.olshevsky on 8/13/15.
 */

import {Router} from 'express'
import RestController from './RestController.js'

class AuthRouter extends RestController {
    constructor(entity, model, metadata) {
        super(entity, model.prebindings);
        this.metadata = metadata;

        this.url = `/${metadata.user}/${metadata.project}/auth/api/:version/${model.alias}`;
        this.router = Router();

        this.router.use(this.url, this.check.bind(this));

        this.router.get(this.url, this.get.bind(this));
        this.router.post(this.url, this.authenticate.bind(this));
        this.router.delete(this.url, this.logout.bind(this));
    }

    check(req, res, next) {
        req.__version = req.params.version
        next()
    }

    authenticate(req, res, next) {
        res.json({success: false, data: {message: "Not implemented yet!"}})
    }

    logout() {
        res.json({success: false, data: {message: "Not implemented yet!"}})
    }

    get(req, res, next) {
        let token = req.headers.token || req.query.token;
        if(!token) {
            res.json({success: false, data: {message: "Invalid Token!"}})
        } else {
            this.entity.findOne({tokens: token}).exec((err, result) => {
                if(!result) {
                    res.json({success: false, data: {message: "Token Not Found!"}})
                } else {
                    res.json(result)
                }
            })
        }
    }
}

export default AuthRouter