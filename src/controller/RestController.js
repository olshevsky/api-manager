/**
 * Created by igor.olshevsky on 8/12/15.
 */
import Controller from './Controller.js'

class RestController extends Controller {
    constructor(entity, prebinding) {
        super();

        this.entity = entity;
        this.prebinding = prebinding || '';
    }

    single(id, req, res, next) {
        this.entity.findById(id).populate(this.prebinding).exec((err, model) => {
            if(err) {
                res.json(err);
            } else {
                res.json(model);
            }
        });
    }

    list(req, res, next) {
        this.entity.find({}).populate(this.prebinding).exec((err, models) => {
            if(err) {
                res.json(models)
            } else {
                res.json(models)
            }
        });
    }

    create(data, req, res, next) {
        let object = new (this.entity)(data);
        object.save((err) => {
            if(err) {
                next(err)
            } else {
                res.json(object)
            }
        })
    }

    update(id, data, req, res, next) {

    }

    patch(id, data, req, res, next) {

    }

    delete(id, req, res, next) {
        this.entity.findById(id).remove().exec((err) => {
            if(err) {
                res.json({success: false})
            } else {
                res.json({id: id, success: true})
            }
        });
    }
}

export default RestController