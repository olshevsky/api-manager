/**
 * Created by igor.olshevsky on 8/12/15.
 */
import {Router} from 'express'
import RestController from './RestController.js'

class ApiCoreController extends RestController {
    constructor(entity, model, metadata = {}) {
        super(entity, model.prebindings);

        this.router = Router();
        this.metadata = metadata;

        this.url = `/${metadata.user}/${metadata.project}/api/:version/${model.alias}/:id?`;

        this.router.use(this.url, this.check.bind(this));
        this.router.get(this.url, this.get.bind(this));
        this.router.post(this.url, this.post.bind(this));
        this.router.put(this.url, this.post.bind(this));
        this.router.patch(this.url, this.patch.bind(this));
        this.router.delete(this.url, this.delete.bind(this));
    }

    check(req, res, next) {
        req.__version = req.params.version
        next()
    }

    get(req, res, next) {
        let id = req.params.id;

        if(id) {
            this.single(req.params.id, req, res, next)
        } else {
            this.list(req, res, next)
        }
    }

    post(req, res, next) {
        let data = req.body,
            id = req.params.id;
        if(id) {
            this.create(data, req, res, next)
        } else {
            this.update(id, data, req, res, next)
        }
    }

    patch(req, res, next) {
        let data = req.body,
            id = req.params.id;
        if(id) {
            this.patch(id, data, req, res, next)
        } else {
            res.json({success: false})
        }
    }

    delete(req, res, next) {
        this.delete(req.params.id, req, res, next)
    }
}

export default ApiCoreController