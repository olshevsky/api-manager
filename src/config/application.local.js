/**
 * Created by igor.olshevsky on 8/13/15.
 */

export default {
    centrifuge: {
        path: './../../Work/Pockemon/centrifugo-0.2.4-linux-amd64',
        web: './../../Work/Pockemon/centrifugo-0.2.4-linux-amd64/web-master/app'
    },
    application: {
        port: 9000
    },
    manager: {
        start: './../api-core/bin/api-core.js'
    }
}
