/**
 * Created by igor.olshevsky on 8/13/15.
 */

export default {
    application: {
        id: "pockemon_manager",
        user: "pockemon_manager",
        project: "api",
        version: 1,
        port: 3000
    },
    manager: {
        start: './node_modules/api-core/bin/api-core'
    }
}