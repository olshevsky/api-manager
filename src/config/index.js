/**
 * Created by igor.olshevsky on 8/13/15.
 */

import application from './application.js'
import local from './application.local.js'
import  fs from 'fs'

function merge(o1, o2) {
    Object.keys(o2).forEach((prop) => {
        var val = o2[prop];
        if (o1.hasOwnProperty(prop)) {
            if (typeof val == 'object') {
                if (val && val.constructor != Array) {
                    val = merge(o1[prop], val);
                }
            }
        }
        o1[prop] = val;
    });
    return o1;
}
export default merge(application, local);