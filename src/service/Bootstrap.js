/**
 * Created by igor.olshevsky on 8/13/15.
 */

import Promise from 'promise'
import localPort from 'local-port'
import Project, {TYPES as ProjectTypes} from './../document/Project.js'
import ProcessManager from './ProcessManager.js'
import moment from 'moment'

class Bootstrap {
    static init() {
        return new Promise((resolve, reject) => {
            Project.find().exec((err, projects) => {
                projects.map(project => {
                    localPort.findOpen(3008, (err, port) => {
                        if(err) {
                            console.error(err)
                        } else {
                            let {user} = project.runner;
                            let projectId = project.runner.project;
                            ProcessManager.create(user, projectId, port);
                        }
                    });
                });

                resolve(projects);
            })
        })
    }
}

export default Bootstrap