/**
 * Created by igor.olshevsky on 8/13/15.
 */

import mongoose from 'mongoose'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

class TypeChecker {
    static convert(type, meta) {
        switch(type) {
            case TypeChecker.TYPES.STRING:
                return {type: String, default: meta.default};
            case TypeChecker.TYPES.NUMBER:
                return {type: Number, default: meta.default};
            case TypeChecker.TYPES.DATE:
                return {type: Date, default: meta.default};
            case TypeChecker.TYPES.ARRAY:
                return {type: Array, default: meta.default};
            case TypeChecker.TYPES.OBJECT:
                return typeof meta.default == 'object' ? {type: Object, default: meta.default} : {};
            case TypeChecker.TYPES.REF:
                return {type: ObjectId, ref: meta.ref};
            case TypeChecker.TYPES.LIST:
                return [{type: ObjectId, ref: meta.ref}];
            case TypeChecker.TYPES.PREBIND:
                return {entity: {type: ObjectId, ref: meta.ref}, metadata: {}};
            case TypeChecker.TYPES.PREBIND_LIST:
                return [{entity: {type: ObjectId, ref: meta.ref}, metadata: {}}];
        }
    }
}

TypeChecker.TYPES = {
    STRING: 'string',
    MIDDLE: 'middle',
    PREBIND: 'prebind',
    PREBIND_LIST: 'prebind_list',
    NUMBER: 'number',
    OBJECT: 'object',
    ARRAY: 'array',
    DATE: 'date',
    REF: 'ref',
    LIST: 'list'
};

export default TypeChecker