/**
 * Created by igor.olshevsky on 8/28/15.
 */
class InstanceLogger {
    constructor() {
        this.instances = {}
    }

    add(instance, message) {
        this.instances[instance] = this.instances[instance] || [];
        this.instances[instance].push(message)
    }

    get() {
        return this.instances;
    }
}

export default new InstanceLogger