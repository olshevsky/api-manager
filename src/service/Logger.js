/**
 * Created by igor.olshevsky on 8/14/15.
 */
import winston from 'winston'
import moment from 'moment'

class Logger {
    constructor() {
        this.logger = new (winston.Logger)({
            transports: [
                new (winston.transports.Console)()
            ]
        });
    }
}

export default new Logger()