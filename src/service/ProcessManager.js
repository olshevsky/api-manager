/**
 * Created by igor.olshevsky on 8/13/15.
 */

import {spawn} from 'child_process'
import config from './../config'
import Logger from './Logger.js'
import InstanceLogger from './InstanceLogger.js'

class ProcessManager {
    constructor() {
        this.list = {};
    }

    get(part) {
        if(!this.list.hasOwnProperty(part)) {
            this.create(part)
        }

        return this.list[part]
    }

    has(part) {
        return this.list.hasOwnProperty(part)
    }

    restart(processName) {
        if(this.has(processName)) {
            try {
                let APIApp = this.get(processName);

                APIApp.process.kill();
                delete APIApp.process;

                let env = {
                    port: APIApp.port,
                    project: APIApp.project,
                    user: APIApp.user,
                    url: APIApp.url
                };

                APIApp.process = ProcessManager.getProcess(env);

                return true;
            } catch(e) {
                return false;
            }
        } else {
            return false;
        }
    }

    getProcessName(user, project) {
        return `/${user}/${project}`;
    }

    create(user, project, port) {
        let url = this.getProcessName(user, project);

        let env = {
            url,
            project,
            user,
            port
        };
        let apiProcess = ProcessManager.getProcess(env);

        this.list[url] = {
            hostname: `http://${process.env.HOSTNAME}:${port}`,
            process: apiProcess,
            project,
            user,
            url,
            logs: [],
            port
        };

        apiProcess.stdout.on('data', function (data) {
            Logger.logger.info(data.toString());
            let parsed = {};
            try {
                parsed = JSON.parse(data.toString())
            } catch(e) {}
            InstanceLogger.add(url, parsed)
        });
        apiProcess.stderr.on("data", function(data) {
            Logger.logger.error(data.toString());
            let parsed = {};
            try {
                parsed = JSON.parse(data.toString())
            } catch(e) {}
            InstanceLogger.add(url, parsed)
        });

        return apiProcess
    }

    static getProcess(env) {
        return spawn('node', [config.manager.start], {env});
    }
}

export default new ProcessManager