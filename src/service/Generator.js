/**
 * Created by igor.olshevsky on 8/13/15.
 */
import TypeChecker from './TypeChecker.js'
import mongoose from 'mongoose'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

class Generator {
    static createSchema(connection, model) {
        let {name, structure, alias} = model;
        let schema = {};
        if(structure) {

            Object.keys(structure).forEach(property => {
                let description = structure[property];
                schema[property] = TypeChecker.convert(description.type, description)
            });

            let Model = new Schema(schema);
            return connection.model(alias, Model);
        } else {
           throw "Schema not found!"
        }
    }
}

export default Generator