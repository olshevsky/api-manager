/**
 * Created by igor.olshevsky on 8/13/15.
 */
import JsonResponse from './JsonResponse.js'

class ErrorResponse extends JsonResponse {
    constructor(message, data, success, status) {
        super(data, success, status);

        this.message = message;
    }

    get data() {
        return {
            message: this.message,
            data: this._data,
            success: this._success
        }
    }
}

export default ErrorResponse