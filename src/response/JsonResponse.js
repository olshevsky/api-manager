/**
 * Created by igor.olshevsky on 8/13/15.
 */
import Response from './Response.js'

class JsonResponse extends Response {
    constructor(data, success, status) {
        super(data, status)
        this._success = success;
    }

    get data() {
        return {
            data: this._data,
            success: this._success
        }
    }

    send() {
        this._response.status(this.status).json(this.data)
    }
}

export default JsonResponse