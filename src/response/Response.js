/**
 * Created by igor.olshevsky on 8/13/15.
 */
class Response {
    constructor(data, status) {
        this._data = data;
        this.status = status;
    }

    set response(req) {
        this._response = req
    }

    get data() {

    }

    send() {
        this._response.status(this.status).send(this._data)
    }
}

export default Response