import {createServer as serve} from 'http-proxy'
import ProcessManager from './../service/ProcessManager.js'
import {globalAgent} from 'https'
import 'http'
import bodyParser from 'body-parser'

export default function(req, res, next) {
    var a = req.url.split('/');
    if (a.length >= 5) {
        var part = `/${a[1]}/${a[2]}`;
        if (ProcessManager.has(part)) {
            let proxy = serve({});
            let instance = ProcessManager.get(part);
            proxy.web(req, res, {
                target: instance.hostname,
                ws: true
            });
        } else {
            next()
        }
    } else {
        next()
    }
}