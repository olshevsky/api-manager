/**
 * Created by igor.olshevsky on 8/13/15.
 */

import express from 'express'
import config from './config'
import {spawn} from 'child_process'
import path from 'path'
import favicon from 'serve-favicon'
import logger from 'morgan'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import fs from 'fs'
import response from './middleware/response.js'
import serverError from './middleware/server-error.js'
import notFound from './middleware/not-found.js'
import proxy from './middleware/proxy.js'
import {Server} from 'http'
import SocketIO from 'socket.io'
import Bootstrap from './service/Bootstrap.js'
import ProcessManager from './service/ProcessManager.js'
import Logger from './service/Logger.js'
import InstanceLogger from './service/InstanceLogger.js'

let {port} = config.application;
let app = express();
let server = Server(app);
let io = SocketIO(server);

io.on('connection', (socket) => {

});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.get('/', (req, res, next) => {
    res.render('index')
});

app.use(response);
app.use(require('cors')());
app.use(proxy);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/restart', function(req, res, next) {
    Object.keys(ProcessManager.list).forEach(p => {
        ProcessManager.restart(p);
    });

    res.json({success: true});
});

app.get('/logs', function(req, res, next) {
    res.json(InstanceLogger.get());
});

app.get('/management', function(req, res, next) {
    res.json(Object.keys(ProcessManager.list).map(p => {
        return {
            p: ProcessManager.list[p].port
        }
    }));
});

app.get('/api/manager/restart/:user/:project', function(req, res, next) {
    let processName = ProcessManager.getProcessName(req.params.user, req.params.project);
    if(ProcessManager.has(processName)) {
        if(ProcessManager.restart(processName)) {
            res.json({success: true, data: {message: `Process ${processName} has been restarted!`}})
        } else {
            res.json({success: false, data: {message: `Process ${processName} can not be restarted!!`}})
        }

    } else {
        res.json({success: false, data: {message: `Process ${processName} not found!`}})
    }
});

Bootstrap.init().then((projects) => {
    let p = projects.map(project => {
        return {
            name: project.runner.project,
            secret: project.runner.secret
        }
    });
});

app.use(notFound);
app.use(serverError);
let p = port || 3001;

Logger.logger.info({success: true, message: "Manager has been started", metadata: {port: p}});
server.listen(p);
